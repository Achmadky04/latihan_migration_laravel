<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        // dd($post);
        return view('casts.index', compact('cast'));
    }

    public function create(){
        return view ('casts.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'nama' => 'required| max:45',
            'umur' => 'required',
            'bio'  => 'required'

        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur"  => $request["umur"],
            "bio"  => $request["bio"]
        ]);

        return redirect('/cast')->with('success','Berhasil di Tambahkan');
    }

    public function show($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();
        return view('casts.show', compact('cast'));
    }

    public function edit($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first();

        return view('casts.edit', compact('cast'));
    }

    public function update($cast_id, Request $request){
        $request->validate([
            'nama' => 'required| max:45',
            'umur' => 'required',
            'bio'  => 'required'

        ]);

        $query = DB::table('cast')->where('id', $cast_id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio']
        ]);

        return redirect('/cast')->with('success', 'Berhasil Edit');
    }

    public function destroy($cast_id){
        $query = DB::table('cast')->where('id' , $cast_id)->delete();

        return redirect('/cast')->with('success' , 'Cast Berhasil di Hapus');
    }

}
