@extends('master')

@section('navbar')
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        {{-- <li class="nav-item d-none d-sm-inline-block">
        <a href="../../index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a> --}}
        </li>
    </ul>
</nav>
@endsection

@section('content')

<div class="mx-4 my-4">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Table Cast</h3>

            <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <a class="btn btn-primary mb-2 " href="/cast/create">Add Cast</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Bio</th>
                        <th style="width : 40px">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key => $casts)
                    <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $casts -> nama }}</td>
                        <td> {{ $casts -> umur }}</td>
                        <td> {{ $casts -> bio }}</td>
                        <td style="display: flex">
                            <a  href="/cast/{{$casts->id}}" class="btn btn-info">Detail</a>
                            <a class="btn btn-warning" href="/cast/{{$casts->id}}/edit">Edit</a>
                            <form action="/cast/{{$casts->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">No Cast Data</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>

</div>

@endsection
